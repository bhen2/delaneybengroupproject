POST http://localhost:8080/claims
Content-Type: application/json

{"customerId": 2,
  "claimNumber": 9,
  "claimDate": "2021-07-09",
  "claimAmount":  1200,
  "status": "Accepted",
  "type": "Auto"}