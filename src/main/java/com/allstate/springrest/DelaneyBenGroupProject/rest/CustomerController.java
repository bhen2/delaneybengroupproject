package com.allstate.springrest.DelaneyBenGroupProject.rest;

import com.allstate.springrest.DelaneyBenGroupProject.entities.Customer;
import com.allstate.springrest.DelaneyBenGroupProject.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/customers")
@CrossOrigin
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping
    public Collection<Customer> getCustomers() {
        return customerService.getAllCustomers();
    }

    @GetMapping(value="/{id}")
    public Customer getCustomerById(@PathVariable("id") int id) {
        return customerService.getCustomerById(id);
    }

    @PutMapping
    public Customer updateCustomerById(@RequestBody Customer updatedCustomer) {
        return customerService.updateCustomerById(updatedCustomer);
    }
    @PostMapping
    public Customer addCustomer(@RequestBody Customer customer) {
        return customerService.addCustomer(customer);
    }

    @DeleteMapping(value = "/{id}")
    public Customer removeCustomer(@PathVariable("id") int id) { return customerService.removeCustomer(id); }

}
