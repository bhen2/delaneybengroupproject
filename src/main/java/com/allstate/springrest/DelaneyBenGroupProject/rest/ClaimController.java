package com.allstate.springrest.DelaneyBenGroupProject.rest;

import com.allstate.springrest.DelaneyBenGroupProject.entities.Claim;
import com.allstate.springrest.DelaneyBenGroupProject.entities.Customer;
import com.allstate.springrest.DelaneyBenGroupProject.service.ClaimService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/claims")
@CrossOrigin

public class ClaimController {

    @Autowired
    private ClaimService claimService;

    @GetMapping
    public Collection<Claim> getClaims() {
        return claimService.getAllClaims();
    }

    @GetMapping(value="/{id}")
    public Claim getClaimById(@PathVariable("id") int id) {
        return claimService.getClaimById(id);
    }

    @PutMapping
    public Claim updateClaimById(@RequestBody Claim updatedClaim) {
        return claimService.updateClaimById(updatedClaim);
    }
    @PostMapping
    public Claim addClaim(@RequestBody Claim claim) {
        return claimService.addClaim(claim);
    }
}
