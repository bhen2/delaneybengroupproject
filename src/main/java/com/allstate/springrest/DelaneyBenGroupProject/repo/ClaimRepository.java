package com.allstate.springrest.DelaneyBenGroupProject.repo;

import com.allstate.springrest.DelaneyBenGroupProject.entities.Claim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClaimRepository extends JpaRepository<Claim, Integer> {
}
