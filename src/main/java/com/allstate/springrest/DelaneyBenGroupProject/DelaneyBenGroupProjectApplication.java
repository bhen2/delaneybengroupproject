package com.allstate.springrest.DelaneyBenGroupProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DelaneyBenGroupProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(DelaneyBenGroupProjectApplication.class, args);
	}

}
