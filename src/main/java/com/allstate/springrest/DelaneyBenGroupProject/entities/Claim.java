package com.allstate.springrest.DelaneyBenGroupProject.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="claims")
public class Claim {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int instanceId;

    @Column(name="cust_id")
    private int customerId;

    @Column(name="claim_num")
    private int claimNumber;

    @Column(name="claim_date")
    private String claimDate;

    @Column(name="claim_amount")
    private double claimAmount;

    @Column(name="status")
    private String status; //Accepted, Rejected, On-going, Pending, Completed

    @Column(name="type")
    private String type; //Priorty: Auto Claims

    public int getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(int instanceId) {
        this.instanceId = instanceId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(int claimNumber) {
        this.claimNumber = claimNumber;
    }

    public String getClaimDate() {
        return claimDate;
    }

    public void setClaimDate(String claimDate) {
        this.claimDate = claimDate;
    }

    public double getClaimAmount() {
        return claimAmount;
    }

    public void setClaimAmount(double claimAmount) {
        this.claimAmount = claimAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
