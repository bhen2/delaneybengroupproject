CREATE TABLE `conygre`.`customers` (
                                       `id` int NOT NULL AUTO_INCREMENT,
                                       `fname` varchar(45) DEFAULT NULL,
                                       `lname` varchar(45) DEFAULT NULL,
                                       `city` varchar(45) DEFAULT NULL,
                                       `zip` int DEFAULT NULL,
                                       `date` varchar(45) DEFAULT NULL,
                                       PRIMARY KEY (`id`));

INSERT INTO `conygre`.`customers` (`id`, `fname`, `lname`, `city`, `zip`, `date`) VALUES ('1', 'John', 'Doe', 'New York', '10001', '2021-07-09');
INSERT INTO `conygre`.`customers` (`id`, `fname`, `lname`, `city`, `zip`, `date`) VALUES ('2', 'Jane', 'Doe', 'New York', '10001', '2021-07-09');
INSERT INTO `conygre`.`customers` (`id`, `fname`, `lname`, `city`, `zip`, `date`) VALUES ('3', 'George', 'Lucas', 'Nicasio', '94946', '2021-07-09');

CREATE TABLE `conygre`.`claims` (
                                    `id` int NOT NULL AUTO_INCREMENT,
                                    `cust_id` int DEFAULT NULL,
                                    `claim_num` int DEFAULT NULL,
                                    `claim_date` varchar(45) DEFAULT NULL,
                                    `claim_amount` double DEFAULT NULL,
                                    `status` varchar(45) DEFAULT NULL,
                                    `type` varchar(45) DEFAULT NULL,
                                    PRIMARY KEY (`id`),
                                    FOREIGN KEY (`cust_id`) REFERENCES customers(`id`) );

INSERT INTO `conygre`.`claims` (`id`, `cust_id`, `claim_num`, `claim_date`, `claim_amount`, `status`, `type`) VALUES ('1', '1', '1', '2021-07-09', '500', 'Accepted', 'Auto');
INSERT INTO `conygre`.`claims` (`id`, `cust_id`, `claim_num`, `claim_date`, `claim_amount`, `status`, `type`) VALUES ('2', '1', '2', '2021-07-10', '1000', 'Rejected', 'Auto');
INSERT INTO `conygre`.`claims` (`id`, `cust_id`, `claim_num`, `claim_date`, `claim_amount`, `status`, `type`) VALUES ('3', '2', '1', '2021-07-11', '10000', 'Pending', 'Auto');
