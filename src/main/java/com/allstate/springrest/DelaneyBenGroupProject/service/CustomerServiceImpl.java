package com.allstate.springrest.DelaneyBenGroupProject.service;

import com.allstate.springrest.DelaneyBenGroupProject.entities.Customer;
import com.allstate.springrest.DelaneyBenGroupProject.repo.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Collection<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    @Override
    public Customer getCustomerById(int id) {
        return customerRepository.findById(id).get();
    }

    @Override
    public Customer updateCustomerById(Customer updatedCustomer) {
        return customerRepository.save(updatedCustomer);
    }

    @Override
    public Customer addCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public Customer removeCustomer(int id) {
        Customer toBeDeleted = getCustomerById(id);
        customerRepository.delete(toBeDeleted);
        return toBeDeleted;
    }


}
