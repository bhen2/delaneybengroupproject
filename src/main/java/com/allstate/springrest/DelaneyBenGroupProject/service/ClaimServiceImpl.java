package com.allstate.springrest.DelaneyBenGroupProject.service;

import com.allstate.springrest.DelaneyBenGroupProject.entities.Claim;
import com.allstate.springrest.DelaneyBenGroupProject.repo.ClaimRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ClaimServiceImpl implements ClaimService {

    @Autowired
    private ClaimRepository claimRepository;

    @Override
    public Collection<Claim> getAllClaims(){
        return claimRepository.findAll();
    }

    @Override
    public Claim getClaimById(int id) {
        return claimRepository.findById(id).get();
    }

    @Override
    public Claim updateClaimById(Claim updatedClaim){
        return claimRepository.save(updatedClaim);
    }

    @Override
    public Claim addClaim(Claim claim) {
        return claimRepository.save(claim);
    }
}
