package com.allstate.springrest.DelaneyBenGroupProject.service;

import com.allstate.springrest.DelaneyBenGroupProject.entities.Claim;

import java.util.Collection;

public interface ClaimService {
    Collection<Claim> getAllClaims();
    Claim getClaimById(int id);
    Claim updateClaimById(Claim updatedClaim);
    Claim addClaim(Claim claim);
}
