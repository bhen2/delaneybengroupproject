package com.allstate.springrest.DelaneyBenGroupProject.service;

import com.allstate.springrest.DelaneyBenGroupProject.entities.Customer;

import java.util.Collection;

public interface CustomerService {
    Collection<Customer> getAllCustomers();
    Customer getCustomerById(int id);
    Customer updateCustomerById(Customer updatedCustomer);
    Customer addCustomer(Customer customer);
    Customer removeCustomer(int id);
}
