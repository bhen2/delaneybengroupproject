import $ from 'jquery';
import React, { useEffect, useState } from 'react'

function CustomerTable() {

    const [data, setData] = useState([]);

    const [addCustomer, setAddCustomer] = useState({
        fname: "",
        lname: "",
        city: "",
        zip: "",
        date: ""
    });

    const [deleteCustomer, setDeleteCustomer] = useState(0);

    const [editCustomer, setEditCustomer] = useState({
        id: "",
        fname: "",
        lname: "",
        city: "",
        zip: "",
        date: ""
    });

    useEffect(() => {
        updateTable();
    },[submitAddCustomer]);

    function updateTable() {
        (async () => {
            const result = await populateTable();
            setData(result);
        })();
    }

    async function populateTable() {
        let response = await fetch('http://localhost:8080/customers');
        let customerTable = await response.json();
        return customerTable;
    }

    async function callAPI(url, method, body = "") {
        let response = await fetch(
            url,
            {
                method: method,
                mode: "cors",
                body: JSON.stringify(body),
                headers: {'Content-Type': 'application/json'}
            }
        );

        let output = await response.json();
        return output;
    }


    function showAddCustomer() {
        // $('#customerAddForm').attr('hidden', false)
        animate('#customerAddForm');
    }

    function hideAddCustomer() {
        //$('#customerAddForm').attr('hidden', true);
        stealth('#customerAddForm');

    }

    function animate(selector){
        if($(selector).attr('class') == 'transform')
        {
            $('.transform').toggleClass('transform-active');
            $('.transform').removeClass('transform');
        }
    }

    function stealth(selector){
        if($(selector).attr('class') == 'transform-active')
        {
            $('.transform-active').toggleClass('transform');
            $('.transform-active').removeClass('transform-active');
        }
    }

    function submitAddCustomer() {
        callAPI('http://localhost:8080/customers', 'POST', addCustomer);
        updateTable();
        hideAddCustomer();
    }

    function showDeleteCustomer() {
        $('#deleteCustomerForm').attr('hidden', false);
    }

    function hideDeleteCustomer() {
        $('#deleteCustomerForm').attr('hidden', true);
    }

    function submitDeleteCustomer() {
        let out = callAPI(`http://localhost:8080/customers/${deleteCustomer}`, 'DELETE');
        hideDeleteCustomer();
    }

    function showEditCustomer() {
        $('#editCustomerForm').attr('hidden', false);
    }

    function hideEditCustomer() {
        $('#editCustomerForm').attr('hidden', true);
    }

    function submitEditCustomer() {
        let out = callAPI(`http://localhost:8080/customers`, 'PUT', editCustomer);
        hideEditCustomer();
    }

    return (
        <>
            <table id="customerTable" class="customers">
                <thead>
                    <tr>
                        <th>Customer ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>City</th>
                        <th>Zip Code</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody id="customerTableBody" class="customers card">
                    {data.map((customer) => (
                        <tr key={customer.id}>
                            <td>{customer.id}</td>
                            <td>{customer.fname}</td>
                            <td>{customer.lname}</td>
                            <td>{customer.city}</td>
                            <td>{customer.zip}</td>
                            <td>{customer.date}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
            <button onClick={showAddCustomer}>Add Customer</button>
            <button onClick={showEditCustomer}>Edit Customer</button>
            <button onClick={showDeleteCustomer}>Delete Customer</button>
            <div id="customerAddForm" class="transform" >
                <label>First Name: </label>
                <input type="text" onChange={(e) => {setAddCustomer({...addCustomer, fname:e.target.value})}}/>
                <label>Last Name: </label>
                <input type="text" onChange={(e) => {setAddCustomer({...addCustomer, lname:e.target.value})}}/>
                <label>City: </label>
                <input type="text" onChange={(e) => {setAddCustomer({...addCustomer, city:e.target.value})}}/>
                <label>Zip Code: </label>
                <input type="number" onChange={(e) => {setAddCustomer({...addCustomer, zip:e.target.value})}}/>
                <br />
                <label>Date: </label>
                <input type="date" onChange={(e) => {setAddCustomer({...addCustomer, date:e.target.value})}}/>
                <br />
                <button onClick={hideAddCustomer}>Cancel</button>
                <button onClick={submitAddCustomer}>Submit</button>
            </div>
            <div id="deleteCustomerForm" hidden>
                <h4>Which customer would you like to delete?</h4><br/>
                <label>Customer ID: </label>
                <input type="number" onChange={(e) => {setDeleteCustomer(e.target.value)}} />
                <br/>
                <button onClick={hideDeleteCustomer}>Cancel</button>
                <button onClick={submitDeleteCustomer}>Delete Customer</button>
            </div>
            <div id="editCustomerForm" hidden>
                <h4>Which customer's information would you like to edit?</h4> <br/>
                <label>Customer ID: </label>
                <input type="number" onChange={(e) => {setEditCustomer({...editCustomer, id:e.target.value})}} />
                <br/>
                <h4>Enter the new information below:</h4> <br/>
                <label>First Name: </label>
                <input type="text" onChange={(e) => {setEditCustomer({...editCustomer, fname:e.target.value})}}/>
                <label>Last Name: </label>
                <input type="text" onChange={(e) => {setEditCustomer({...editCustomer, lname:e.target.value})}}/>
                <label>City: </label>
                <input type="text" onChange={(e) => {setEditCustomer({...editCustomer, city:e.target.value})}}/>
                <label>Zip Code: </label>
                <input type="number" onChange={(e) => {setEditCustomer({...editCustomer, zip:e.target.value})}}/>
                <br />
                <label>Date: </label>
                <input type="date" onChange={(e) => {setEditCustomer({...editCustomer, date:e.target.value})}}/>
                <br />
                <button onClick={hideEditCustomer}>Cancel</button>
                <button onClick={submitEditCustomer}>Submit</button>
                
            </div>
        </>    
    )
}

export default CustomerTable