import $ from 'jquery';
import React, { useEffect, useState } from 'react'

function ClaimTable() {
    const [addClaim, setAddClaim] =  useState({
        customerId: '',
        claimNumber: '',
        claimDate: '',
        claimAmount: '',
        status: 'Accepted',
        type: 'Auto'
    })

    const [data, setData] = useState([]);

    const [editClaim, setEditClaim] = useState({
        instanceId: '',
        customerId: '',
        claimNumber: '',
        claimDate: '',
        claimAmount: '',
        status: 'Accepted',
        type: 'Auto'
    })


    useEffect(() => {
        updateTable();
    }, [submitAddClaim]);

    function updateTable() {
        (async () => {
            const result = await populateTable();
            setData(result);
        })();
    }

     async function populateTable() {

        //callAPI('http://localhost:8080/claims', 'GET', {});
        let response = await fetch('http://localhost:8080/claims');
        let claimTable = await response.json();
        return claimTable;
    }

    async function callAPI(url, method, body) {
        let response = await fetch(
            url,
            {
                method: method,
                body: JSON.stringify(body),
                headers: {'Content-Type': 'application/json'}
            }

        );
        let output = await response.json();
        return output;

    }

    function showAddClaim() {
        // $('#claimAddForm').attr('hidden', false);
        animate();
    }

    function hideAddClaim() {
        // $('#claimAddForm').attr('hidden', true);
        stealth();
    }

    function animate(){
        if($('#claimAddForm').attr('class') == 'transform-claim')
        {
            $('.transform-claim').toggleClass('transform-active-claim');
            $('.transform-claim').removeClass('transform-claim');
        }
    }

    function stealth(){
        if($('#claimAddForm').attr('class') == 'transform-active-claim')
        {
            $('.transform-active-claim').toggleClass('transform-claim');
            $('.transform-active-claim').removeClass('transform-active-claim');
        }
    }

    function submitAddClaim() {
        callAPI('http://localhost:8080/claims', 'POST', addClaim);
        updateTable();
        hideAddClaim();
    }

    function showEditClaim() {
        $("#editClaimForm").attr('hidden', false);
    }

    function hideEditClaim() {
        $('#editClaimForm').attr('hidden', true);
    }

    function submitEditClaim() {
        callAPI('http://localhost:8080/claims', 'PUT', editClaim);
        hideEditClaim();
    }


    return (
        <>
            <table id="claimTable" class="customers">
                <thead>
                    <tr>
                        <th>Customer ID</th>
                        <th>Claim Number</th>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody id="claimTableBody" class="customers card">{data.map((claim) => (
                    <tr key={claim.id}>
                        <td>{claim.customerId}</td>
                        <td>{claim.claimNumber}</td>
                        <td>{claim.claimDate}</td>
                        <td>{claim.claimAmount}</td>
                        <td>{claim.status}</td>
                        <td>{claim.type}</td>
                    </tr>
                ))}</tbody>
            </table>
            <button onClick={showAddClaim}>Add Claim</button>
            <button onClick={showEditClaim}>Edit Claim</button>
            <div id="claimAddForm" class="transform-claim">
                <label>Customer ID: </label>
                <input type="number" onChange={(e) => {setAddClaim({...addClaim, customerId: e.target.value})}}/>
                <label>Claim Number: </label>
                <input type="number" onChange={(e) => {setAddClaim({...addClaim, claimNumber: e.target.value})}}/>
                <label>Date: </label>
                <input type="date" onChange={(e) => {setAddClaim({...addClaim, claimDate: e.target.value})}}/>
                <label>Amount: </label>
                <input placeholder="Claim Amount" type="number" onChange={(e) => {setAddClaim({...addClaim, claimAmount: e.target.value})}}/>
                <br />
                <label>Status: </label>
                <select onChange={(e) => {setAddClaim({...addClaim, status: e.target.value})}}>
                    <option value="Accepted">Accepted</option>
                    <option value="Rejected">Rejected</option>
                    <option value="On-Going">On-Going</option>
                    <option value="Completed">Completed</option>
                </select>
                <label>Claim Type: </label>
                <select onChange={(e) => {setAddClaim({...addClaim, type: e.target.value})}}>
                    <option value="auto">Auto</option>
                </select>
                <br />
                <button onClick={hideAddClaim}>Cancel</button>
                <button onClick={submitAddClaim}>Submit</button>
            </div>
            <div id="editClaimForm" hidden>
            <h4>Which claim information would you like to edit?</h4> <br/>
                <label>Claim ID: </label>
                <input type="number" onChange={(e) => {setEditClaim({...editClaim, instanceId:e.target.value})}} />
                <br/>
                <h4>Enter the new information below:</h4> <br/>
                <label>Customer ID: </label>
                <input type="number" onChange={(e) => {setEditClaim({...editClaim, customerId: e.target.value})}}/>
                <label>Claim Number: </label>
                <input type="number" onChange={(e) => {setEditClaim({...editClaim, claimNumber: e.target.value})}}/>
                <label>Date: </label>
                <input type="date" onChange={(e) => {setEditClaim({...editClaim, claimDate: e.target.value})}}/>
                <label>Amount: </label>
                <input placeholder="Claim Amount" type="number" onChange={(e) => {setEditClaim({...editClaim, claimAmount: e.target.value})}}/>
                <br />
                <label>Status: </label>
                <select onChange={(e) => {setEditClaim({...editClaim, status: e.target.value})}}>
                    <option value="Accepted">Accepted</option>
                    <option value="Rejected">Rejected</option>
                    <option value="On-Going">On-Going</option>
                    <option value="Completed">Completed</option>
                </select>
                <label>Claim Type: </label>
                <select onChange={(e) => {setEditClaim({...editClaim, type: e.target.value})}}>
                    <option value="auto">Auto</option>
                </select>
                <br />
                <button onClick={hideEditClaim}>Cancel</button>
                <button onClick={submitEditClaim}>Submit</button>
            </div>
        </>
    )
}

export default ClaimTable