import logo from './logo.svg';
import './App.css';
import CustomerTable from './components/CustomerTable';
import ClaimTable from './components/ClaimTable';

function App() {
  return (
      <div class="bg">
    <div class="center">
      <h2>Customers</h2>
      <CustomerTable />
      <br />
      <br /><br /><br /><br />
      <h2>Claims</h2>
      <ClaimTable />
    </div>
      </div>
  );
}

export default App;
